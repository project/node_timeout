Node Timeout
================================================================================

Have you ever wanted to block a user from a specific node for a short time in 
an automated way? That's what this module is for.  Use the packaged Rule or 
invoke the module's methods directly to set a short-term access block on a 
specific node for a specific user. After the timeout period is cleared the 
user will have access restored automatically.

If you need more nuanced access blocking capabilities (e.g.: via roles, groups, 
taxonomies, etc.), there are plenty of other modules to do that work. Use this 
module to do things like prevent a user from repeating a quiz, filling out a form, 
or other node-based tasks for a short time.


Installation
--------------------------------------------------------------------------------

Do the usual download and place in the `modules` directory process, or if you
are using composer: `$ composer require 'drupal/node_timeout'`.

When the code is ready, use Drush or the UI to install the module as usual.


Configuration
--------------------------------------------------------------------------------

This module requires either an invokation via Rules or via code in a custom module.
No other configurations are required.

When a timeout is detected, the module will invoke the default "Access denied"
page. You may modify this flow or perform other tasks using hook_timeout_enforce().


Additional Notes
--------------------------------------------------------------------------------

The access check is currently performed via hook_node_access; therefore, 
the superuser account (ID = 1) won't be affected by timeouts set via this module.

Future iterations of this module may include custom hooks to allow further actions 
when a timeout is set or invoked for a user.


Maintainers
--------------------------------------------------------------------------------

Current maintainers:

* Tom Kiehne (tkiehne) - https://www.drupal.org/u/tkiehne


Issues
--------------------------------------------------------------------------------

If you have issues with this module you are welcome to submit issues at
https://www.drupal.org/project/node_timeout (all submitted issues are public).
