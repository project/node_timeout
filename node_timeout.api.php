<?php

/**
 * @file
 * Hooks provided by the Node Timeout module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Respond to a timeout being enforced.
 *
 * @param $expire
 *   A DateTime object representing the expiry time for the timeout.
 *
 * @see node_timeout_node_access()
 */
function hook_timeout_enforce($expire) {
  drupal_set_message(t('You have been locked out of this page. The lockout will expire at @t', array('@t' => $expire->format('Y-m-d H:i:s'))), 'warning');
}

/**
 * @} End of "addtogroup hooks".
 */
