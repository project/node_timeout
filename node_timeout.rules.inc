<?php
 
/**
 * Implement hook_rules_action_info().
 */
function node_timeout_rules_action_info() {
  return array( 
    'node_timeout_rules_action_set_node_timeout' => array(
      'label' => t('Lock user out of Node for a set time'),
      'group' => t('Node'),
      'parameter' => array(
        'nid' => array(
          'type' => 'integer',
          'label' => t('Node'),
          'description' => t('Provide the node ID of the node to lock out.'),
        ),
        'account' => array(
          'type' => 'user',
          'label' => t('User account'),
          'description' => t('Indicate the user account to lock out.'),
        ),
        'expiration' => array(
          'type' => 'date',
          'label' => t('Expiration'),
          'description' => t('Provide a DateTime object to set the expiration time.'),
        ),
      ),
    )
  );
}

function node_timeout_rules_action_set_node_timeout($nid, $account, $expiration) {
  if (is_numeric($expiration)) {
    $expiration = new DateTime('@' . $expiration);
  }

  node_timeout_set_timeout_expiration($nid, $expiration, $account);
}
