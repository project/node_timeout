<?php
/**
 * @file
 * Class to manage node timout data for a user.
 */
 
class TimeoutData {
  /**
   * The associated user account.
   */
  protected $account;

  /**
   * The key for accessing data.
   */
  protected $dataKey = 'timeout';
  protected $colKey = 'node_timeouts';

  /**
   * Constructor for Timeout data object.
   * @param $account
   * @throws \Exception
   */
  public function __construct($account) {
    if (!isset($account->data)) {
      throw new Exception("Malformed user added.");
    }
    $this->account = $account;
  }

  /**
   * Get a data value.
   *
   * @param string $key
   *   The key to get the value using.
   *
   * @return mixed
   *   The value retrieved.
   */
  protected function getDataValue($key) {
    $value = NULL;
    if (
      !empty($this->account->data[$this->dataKey]) &&
      !empty($this->account->data[$this->dataKey][$key])
    ){
      $value = $this->account->data[$this->dataKey][$key];
    }
    return $value;
  }

  /**
   * Get timeout collection.
   *
   * @return array
   *   The array of collection retrieved or NULL if none.
   */
  protected function getTimeouts() {
    return $this->getDataValue($this->colKey);
  }

  /**
   * Set timeout collection.
   *
   * @param array $values
   *   The collection to store.
   *
   */
  protected function setTimeouts($values) {

    if(is_array($values)) {
      $this->account->data[$this->dataKey][$this->colKey] = $values;
    }

  }

  /**
   * Get a node timeout data value.
   *
   * @param string $nid
   *   The node ID to check.
   *
   * @return DateTime
   *   The value retrieved or NULL if none.
   */
  public function getNodeTimeout($nid) {
    $value = NULL;
    $node_timeout = $this->getDataValue($this->colKey);
    if (!empty($node_timeout[$nid])) {
      $value = $node_timeout[$nid];
    }

    if (!empty($value)) {
      $value = new DateTime('@' . $value);
    }

    return $value;
  }

  /**
   * Set a node timeout data value.
   *
   * @param string $nid
   *   The node ID to set.
   * @param string $expiration
   *   The timestamp expiration set.
   */
  public function setNodeTimeout($nid, DateTime $expiration) {

    if (!is_numeric($nid)) {
      throw new InvalidArgumentException('Node ID provided is not numeric');
    }

    if (!is_a($expiration, 'Datetime')) {
      throw new InvalidArgumentException('Expiration provided is not an instance of DateTime');
    }

    $now = new DateTime("now");
    if (isset($this->account->data[$this->dataKey][$this->colKey][$nid]) && $expiration <= $now) {
      $this->clearNodeTimeout($nid);
    }
    else {
      $this->account->data[$this->dataKey][$this->colKey][$nid] = $expiration->getTimestamp();
    }
  }

  /**
   * Clear a node timeout data value.
   *
   * @param string $nid
   *   The node ID to set.
   * @param string $expiration
   *   The timestamp expiration set.
   */
  public function clearNodeTimeout($nid) {
    if (!is_numeric($nid)) {
      throw new InvalidArgumentException('Node ID provided is not numeric');
    }

    unset($this->account->data[$this->dataKey][$this->colKey][$nid]);
  }
}